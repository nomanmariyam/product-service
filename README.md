## How to run the program
#### Prerequisite
	1. MAVEN
	2. JAVA8
#### Clone the repo
```sh
$ git clone https://nomanmariyam@bitbucket.org/nomanmariyam/product-service.git
```
#### Build the project
```
$ cd product-service 
$ mvn clean install
```
#### Start the service
```sh
$ java -jar product-service-app/target/product-service-app.jar
```
 Default port is 8080, to change it execute the below command
 
```sh
$ java -jar product-service-app/target/product-service-app.jar --server.port=8090
```
-----------------------------------------------
### Docker 
Alternate there is also docker image available in docker hub
#### Get the image
```sh
$ docker pull nomanmariyam/product-service-app:R1.0-SNAPSHOT
```
#### Start the container
```sh
$ docker run -p 8080:8080  nomanmariyam/product-service-app:R1.0-SNAPSHOT
```
-----------------------------------------------

#### Test the service
under following url , swagger docs are available for the service

http://localhost:8080/product-service/swagger-ui.html

### Use below curl commands to test the service

#### Create Or Update Product

``` 
  curl -X POST \
  http://localhost:8090/product-service/products \
  -H 'Content-Type: application/json' \
  -d '{	"name":"p1",
	"price":20
}'
```

#### Fetch the  Product

```
  curl -X GET \
  http://localhost:8090/product-service/products \
  -H 'cache-control: no-cache'
```
#### Create the Order

```
curl -X POST \
  http://localhost:8090/product-service/order \
  -H 'Content-Type: application/json' \
  -d '{
  "buyerEmail": "email1",
  "products": [
    "p1"
  ]
}\
'
```

#### Fetch All Orders
```
  curl -X GET \
  http://localhost:8090/product-service/order \
  -H 'cache-control: no-cache'
```

#### Fetch Orders between time range
```
 curl -X GET \
  'http://localhost:8090/product-service/fetchOrders?end=2018-12-10&start=2018-12-01' \
  -H 'cache-control: no-cache'
```
-------------------------------------------------
### The project has been implemented using spring-boot, contains below 4 modules

#### product-service-api  
  interface containing service endpoint and response models
#### product-service-impl 
service implementation
#### product-service-db 
database layer (db models, repo)
#### product-service-app
App entry point, spring boot configuration