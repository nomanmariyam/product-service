package com.noman.product.db.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.noman.product.db.entity.OrderModel;
import com.noman.product.db.entity.ProductModel;
import com.noman.product.db.entity.ProductPriceModel;
import com.noman.product.db.repository.OrderRepository;
import com.noman.product.db.service.exception.ProductServiceDbException;

@RunWith(MockitoJUnitRunner.class)
public class DefaultOrderDbServiceTest {
	
	@InjectMocks
	private DefaultOrderDbService defaultOrderDbService;
	
	@Mock
	private OrderRepository orderRepository;
	
	@Mock
	private ProductDbService productDbService;
	
	@Test
	public void testCreateOrderSuccess() throws ProductServiceDbException {
		List<String> productList = Arrays.asList(StringUtils.split("p1,p2", ","));
		
		Mockito.when(productDbService.getProductByName("p1")).thenReturn(
				Optional.of(new ProductModel("p1", 12.0)));
		
		Mockito.when(productDbService.getProductByName("p2")).thenReturn(
				Optional.of(new ProductModel("p2", 10.0)));
		
		OrderModel orderModel = new OrderModel("email");
		List<ProductPriceModel> productPrices = new ArrayList<ProductPriceModel>();
		productPrices.add(new ProductPriceModel(12.0));
		productPrices.add(new ProductPriceModel(10.0));
		orderModel.setProductsPrices(productPrices);
		
		Mockito.when(orderRepository.save((OrderModel)Mockito.any())).thenReturn(orderModel);
		
		OrderModel createdOrder = defaultOrderDbService.createOrder(productList, "email");
		assertNotNull(createdOrder);
		assertEquals("email", createdOrder.getBuyerEmail());
		assertEquals(2, createdOrder.getProductsPrices().size());
		
		Mockito.verify(productDbService, Mockito.times(1)).getProductByName("p1");
		Mockito.verify(productDbService, Mockito.times(1)).getProductByName("p2");
		Mockito.verify(orderRepository, Mockito.times(1)).save((OrderModel)Mockito.any());
	}
	
	@Test(expected=ProductServiceDbException.class)
	public void testCreateOrderWhenProductNotFound() throws ProductServiceDbException {
		List<String> productList = Arrays.asList(StringUtils.split("p1", ","));
		
		Mockito.when(productDbService.getProductByName("p1")).thenReturn(
				Optional.empty());
		
		OrderModel orderModel = new OrderModel("email");
		List<ProductPriceModel> productPrices = new ArrayList<ProductPriceModel>();
		productPrices.add(new ProductPriceModel(12.0));
		productPrices.add(new ProductPriceModel(10.0));
		orderModel.setProductsPrices(productPrices);
		
		Mockito.when(orderRepository.save((OrderModel)Mockito.any())).thenReturn(orderModel);
		
		defaultOrderDbService.createOrder(productList, "email");
		
		Mockito.verify(productDbService, Mockito.times(1)).getProductByName("p1");
	}
	
	@Test
	public void testGetAllOrdersBetween() throws ProductServiceDbException {
		List<OrderModel> orderModelList = new ArrayList<OrderModel>();
		orderModelList.add(new OrderModel("email"));
		
		Date start = new Date();
		Date end = new Date();
		
		Mockito.when(
				orderRepository.
				findAllByCreateDateGreaterThanEqualAndCreateDateLessThanEqual(start, end))
				.thenReturn(orderModelList);
		
		List<OrderModel> fetchedOrders = defaultOrderDbService.getAllOrdersBetween(start, end);
		assertNotNull(fetchedOrders);
		assertEquals(1, fetchedOrders.size());
		
		Mockito.verify(orderRepository, Mockito.times(1))
			.findAllByCreateDateGreaterThanEqualAndCreateDateLessThanEqual(start, end);
	}
	
	@Test
	public void testgetAllOrders() throws ProductServiceDbException {
		List<OrderModel> orderModelList = new ArrayList<OrderModel>();
		orderModelList.add(new OrderModel("email"));
		
		Mockito.when(orderRepository.findAll())
				.thenReturn(orderModelList);
		
		List<OrderModel> fetchedOrders = defaultOrderDbService.getAllOrders();
		assertNotNull(fetchedOrders);
		assertEquals(1, fetchedOrders.size());
		
		Mockito.verify(orderRepository, Mockito.times(1)).findAll();
	}
}
