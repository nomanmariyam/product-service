package com.noman.product.db.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.noman.product.db.entity.PriceStatus;
import com.noman.product.db.entity.ProductModel;
import com.noman.product.db.entity.ProductPriceModel;
import com.noman.product.db.repository.ProductRepository;

@RunWith(MockitoJUnitRunner.class)
public class DefaultProductDbServiceTest {
	
	@InjectMocks
	private DefaultProductDbService defaultProductDbService;
	
	@Mock
	private ProductRepository productRepository;
	
	@Test
	public void testGetAllProducts() {
		List<ProductModel> productList = prepareProductModels();
		Mockito.when(productRepository.findAll()).thenReturn(productList);
		
		List<ProductModel> fetchedProducts = defaultProductDbService.getAllProducts();
		assertNotNull(fetchedProducts);
		assertEquals(productList.size(), fetchedProducts.size());
		assertEquals(productList.get(0), fetchedProducts.get(0));
		assertEquals(productList.get(1), fetchedProducts.get(1));
		
		Mockito.verify(productRepository, Mockito.times(1)).findAll();
	}
	
	@Test
	public void testCreateOrUpdateProductWhenCreate() {
		Mockito.when(productRepository.findByName("product1")).thenReturn(Optional.empty());
		ProductModel productModel = new ProductModel("product1", 10.0);
		Mockito.when(productRepository.save((ProductModel)Mockito.any())).thenReturn(productModel);
		
		ProductModel createdOrUpdatedProduct = 
				defaultProductDbService.createOrUpdateProduct("product1", 10.0);
		assertNotNull(createdOrUpdatedProduct);
		assertNotNull(createdOrUpdatedProduct);
		assertEquals("product1", createdOrUpdatedProduct.getName());
		assertEquals(productModel, createdOrUpdatedProduct);
		
		Mockito.verify(productRepository, Mockito.times(1)).findByName("product1");
		Mockito.verify(productRepository, Mockito.times(1)).save((ProductModel)Mockito.any());
	}
	
	@Test
	public void testCreateOrUpdateProductWhenUpdate() {
		ProductModel productModel = new ProductModel("product1", 10.0);
		
		Mockito.when(productRepository.findByName("product1")).thenReturn(
				Optional.of(productModel));
		
		Mockito.when(productRepository.save((ProductModel)Mockito.any())).
			thenReturn(productModel);
		
		ProductModel createdOrUpdatedProduct = 
				defaultProductDbService.createOrUpdateProduct("product1", 12.0);
		
		assertNotNull(createdOrUpdatedProduct);
		assertEquals("product1", createdOrUpdatedProduct.getName());
		
		List<ProductPriceModel> updatedProductPrice = createdOrUpdatedProduct.getProductPrice();
		assertEquals(2, updatedProductPrice.size());
		assertEquals(10.0, updatedProductPrice.get(0).getPrice(), 0);
		assertEquals(PriceStatus.HISTORY.name(), updatedProductPrice.get(0).getPriceStatus());
		
		assertEquals(12.0, updatedProductPrice.get(1).getPrice(), 0);
		assertEquals(PriceStatus.CURRENT.name(), updatedProductPrice.get(1).getPriceStatus());
		
		Mockito.verify(productRepository, Mockito.times(1)).findByName("product1");
		Mockito.verify(productRepository, Mockito.times(1)).save((ProductModel)Mockito.any());
	}
	
	@Test
	public void testGetProductByName() {
		Mockito.when(productRepository.findByName("product1")).thenReturn(
				Optional.of(new ProductModel("product1", 12.0)));
		
		Optional<ProductModel> fetchedProduct = defaultProductDbService.getProductByName("product1");
		assertTrue(fetchedProduct.isPresent());
		assertNotNull(fetchedProduct.get());
		assertEquals("product1", fetchedProduct.get().getName());
		assertEquals(12.0, fetchedProduct.get().getProductPrice().get(0).getPrice(), 0);
		
		Mockito.verify(productRepository, Mockito.times(1)).findByName("product1");
	}

	private List<ProductModel> prepareProductModels() {
		List<ProductModel> productModels = new ArrayList<ProductModel>();
		productModels.add(new ProductModel("product1", 10.0));
		productModels.add(new ProductModel("product2", 10.0));
		return productModels;
	}
	
}
