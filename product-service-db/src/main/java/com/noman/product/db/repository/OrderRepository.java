package com.noman.product.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.noman.product.db.entity.OrderModel;

@Repository
public interface OrderRepository extends JpaRepository<OrderModel, Long> {

	List<OrderModel> findAllByCreateDateGreaterThanEqualAndCreateDateLessThanEqual(Date startDate, Date endDate);
}
