package com.noman.product.db.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.noman.product.db.entity.ProductModel;
import com.noman.product.db.repository.ProductRepository;

@Service
public class DefaultProductDbService implements ProductDbService {
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<ProductModel> getAllProducts() {
		return productRepository.findAll();
	}

	@Override
	public ProductModel createOrUpdateProduct(String name, Double price) {
		Optional<ProductModel> productModelOptional = productRepository.findByName(name);
		ProductModel productModel = productModelOptional.orElse(new ProductModel());
		productModel.setName(name);
		productModel.setProductCurrentPrice(price);
		return productRepository.save(productModel);
	}

	@Override
	public Optional<ProductModel> getProductByName(String name) {
		return productRepository.findByName(name);
	}
}
