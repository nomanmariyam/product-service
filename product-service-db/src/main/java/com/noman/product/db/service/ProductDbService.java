package com.noman.product.db.service;

import java.util.List;
import java.util.Optional;

import com.noman.product.db.entity.ProductModel;

public interface ProductDbService {
	
	ProductModel createOrUpdateProduct(String name, Double price);
	
	Optional<ProductModel> getProductByName(String name);
	
	List<ProductModel> getAllProducts();
	
}
