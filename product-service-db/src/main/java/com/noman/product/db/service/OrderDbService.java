package com.noman.product.db.service;

import java.util.Date;
import java.util.List;

import com.noman.product.db.entity.OrderModel;
import com.noman.product.db.service.exception.ProductServiceDbException;

public interface OrderDbService {
	
	OrderModel createOrder(List<String> products, String buyerEmail) throws ProductServiceDbException;
	
	List<OrderModel> getAllOrders() throws ProductServiceDbException;
	
	List<OrderModel> getAllOrdersBetween(Date start, Date end) throws ProductServiceDbException;
}
