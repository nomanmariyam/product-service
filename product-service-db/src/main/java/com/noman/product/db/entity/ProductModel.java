package com.noman.product.db.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODUCT")
public class ProductModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "PRODUCT_ID")
	private Long productId;
	
	@Column(unique=true, name="NAME")
	private String name;
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProductPriceModel> productPrice = new ArrayList<ProductPriceModel>();
	
	@Column(name = "CREATE_DATE")
    private Date createDate;
	
	public ProductModel() {
		super();
	}
	
	public ProductModel(String name, Double price) {
		super();
		this.name = name;
		this.createDate = new Date();
		ProductPriceModel productPrice = new ProductPriceModel(price);
		productPrice.setProduct(this);
		this.productPrice.add(productPrice);
	}

	public Long getProductId() {
		return productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<ProductPriceModel> getProductPrice() {
		return productPrice;
	}
	
	public Optional<ProductPriceModel> getProductCurrentPriceModel() {
		return this.productPrice
			.stream()
			.filter(productPrice -> 
					productPrice.getPriceStatus().equalsIgnoreCase(PriceStatus.CURRENT.name()))
			.findAny();
	}
	
	public Double getProductCurrentPrice() {
		return this.productPrice
			.stream()
			.filter(productPrice -> 
					productPrice.getPriceStatus().equalsIgnoreCase(PriceStatus.CURRENT.name()))
			.findAny().orElse(new ProductPriceModel()).getPrice();
	}

	public void setProductCurrentPrice(Double price) {
		this.productPrice.forEach(productPrice -> 
			productPrice.setPriceStatus(PriceStatus.HISTORY));
		ProductPriceModel productPrice = new ProductPriceModel(price);
		productPrice.setProduct(this);
		this.productPrice.add(productPrice);
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((productPrice == null) ? 0 : productPrice.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductModel other = (ProductModel) obj;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		}
		else if (!createDate.equals(other.createDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		if (productPrice == null) {
			if (other.productPrice != null)
				return false;
		}
		else if (!productPrice.equals(other.productPrice))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		}
		else if (!productId.equals(other.productId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductModel [productId=" + productId + ", name=" + name + ", price=" + productPrice + ", date=" + createDate + "]";
	}
}
