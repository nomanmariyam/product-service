package com.noman.product.db.entity;

public enum PriceStatus {
	  CURRENT,
	  HISTORY
}
