package com.noman.product.db.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.noman.product.db.entity.ProductModel;

@Repository
public interface ProductRepository extends JpaRepository<ProductModel, Long> {
	
	Optional<ProductModel> findByName(String name);
	
}
