package com.noman.product.db.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name="PRODUCT_ORDER")
public class OrderModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ORDER_ID")
	private Long orderId;
	
	@Column(name = "BUYER_EMAIL")
	private String buyerEmail;
	
	@ManyToMany(fetch = FetchType.LAZY)
	private List<ProductPriceModel> productsPrices = new ArrayList<ProductPriceModel>();
	
	@Column(name = "CREATE_DATE")
    private Date createDate;
	
	public OrderModel() {
		super();
		this.createDate = new Date();
	}
	
	public OrderModel(String buyerEmail) {
		super();
		this.buyerEmail = buyerEmail;
		this.createDate = new Date();
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	
	public List<ProductPriceModel> getProductsPrices() {
		return productsPrices;
	}
	
	public Double getTotalPrice() {
		 return productsPrices.stream().mapToDouble(productPrice -> productPrice.getPrice()).sum();
	}
	
	public List<String> getProducts() {
		 return productsPrices.stream().map(
				 productPrice -> productPrice.getProduct().getName()).collect(Collectors.toList());
	}

	public void setProductsPrices(List<ProductPriceModel> productsPrices) {
		this.productsPrices = productsPrices;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createdDate) {
		this.createDate = createdDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buyerEmail == null) ? 0 : buyerEmail.hashCode());
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((productsPrices == null) ? 0 : productsPrices.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderModel other = (OrderModel) obj;
		if (buyerEmail == null) {
			if (other.buyerEmail != null)
				return false;
		}
		else if (!buyerEmail.equals(other.buyerEmail))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		}
		else if (!createDate.equals(other.createDate))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		}
		else if (!orderId.equals(other.orderId))
			return false;
		if (productsPrices == null) {
			if (other.productsPrices != null)
				return false;
		}
		else if (!productsPrices.equals(other.productsPrices))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderModel [orderId=" + orderId + ", buyerEmail=" + buyerEmail + ", productsPrices=" + productsPrices
				+ ", createDate=" + createDate + "]";
	}
}
