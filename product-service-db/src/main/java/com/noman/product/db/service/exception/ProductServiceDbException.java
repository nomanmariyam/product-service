package com.noman.product.db.service.exception;


public class ProductServiceDbException extends Exception {

    private static final long serialVersionUID = 1L;
    
    private String action;

    public ProductServiceDbException(String action, String message, Throwable cause) {
        super(message, cause);
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    /**
     * Constructor to use when another exception should be wrapped.
     * @param t The exception to wrap.
     */
    public ProductServiceDbException(Throwable t) {
        super(t);
    }

    /**
     * Default constructor.
     */
    public ProductServiceDbException() {
        super();
    }

    /**
     * Constructor to use when the exception should contain additional info.
     * @param message The additional info to include.
     */
    public ProductServiceDbException(String message) {
        super(message);
    }

    /**
     * Constructor to wrap an exception and include additional info.
     * @param message The additional info to include.
     * @param t The exception to wrap.
     */
    public ProductServiceDbException(String message, Throwable t) {
        super(message, t);
    }
}
