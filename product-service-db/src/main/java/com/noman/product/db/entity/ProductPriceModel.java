package com.noman.product.db.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRODUCT_PRICE")
public class ProductPriceModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "PRODUCT_PRICE_ID")
	private Long productPriceId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_ID")
	private ProductModel product;
	
	@Column(name = "PRICE")
	private Double price;
	
	@Column(name = "PRICE_STATUS")
	private String priceStatus;
	
	@Column(name = "CREATE_DATE")
    private Date createDate;
	
	public ProductPriceModel() {
		super();
	}
	
	public ProductPriceModel(Double price) {
		super();
		this.price = price;
		this.priceStatus = PriceStatus.CURRENT.name();
		this.createDate = new Date();
	}

	public ProductModel getProduct() {
		return product;
	}

	public void setProduct(ProductModel product) {
		this.product = product;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPriceStatus() {
		return priceStatus;
	}

	public void setPriceStatus(PriceStatus priceStatus) {
		this.priceStatus = priceStatus.name();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date date) {
		this.createDate = date;
	}

	public Long getProductPriceId() {
		return productPriceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((priceStatus == null) ? 0 : priceStatus.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((productPriceId == null) ? 0 : productPriceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductPriceModel other = (ProductPriceModel) obj;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		}
		else if (!createDate.equals(other.createDate))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		}
		else if (!price.equals(other.price))
			return false;
		if (priceStatus == null) {
			if (other.priceStatus != null)
				return false;
		}
		else if (!priceStatus.equals(other.priceStatus))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		}
		else if (!product.equals(other.product))
			return false;
		if (productPriceId == null) {
			if (other.productPriceId != null)
				return false;
		}
		else if (!productPriceId.equals(other.productPriceId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductPriceModel [price=" + price + ", priceStatus=" + priceStatus + ", date=" + createDate + "]";
	}

}
