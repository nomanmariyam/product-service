package com.noman.product.db.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.noman.product.db.entity.OrderModel;
import com.noman.product.db.repository.OrderRepository;
import com.noman.product.db.service.exception.ProductServiceDbException;

@Service
public class DefaultOrderDbService implements OrderDbService {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private ProductDbService productDbService;

	@Override
	public OrderModel createOrder(List<String> products, String buyerEmail) throws ProductServiceDbException {
		OrderModel orderModel = new OrderModel(buyerEmail);
		products.forEach(product -> {
			productDbService.getProductByName(product).ifPresent(productModel -> {
				productModel.getProductCurrentPriceModel().ifPresent(
						productCurrentPrice -> orderModel.getProductsPrices().add(productCurrentPrice));
			});
		});
		
		if(orderModel.getProductsPrices().isEmpty()) {
			throw new ProductServiceDbException("Order can not be created because no product found");	
		}
		return orderRepository.save(orderModel);
	}

	@Override
	public List<OrderModel> getAllOrdersBetween(Date startDate, Date endDate) throws ProductServiceDbException {
		return orderRepository
				.findAllByCreateDateGreaterThanEqualAndCreateDateLessThanEqual(startDate, endDate);
	}

	@Override
	public List<OrderModel> getAllOrders() throws ProductServiceDbException {
		return orderRepository.findAll();
	}
}
