package com.noman.product.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.noman.product.db.entity.ProductModel;
import com.noman.product.db.service.ProductDbService;
import com.noman.product.service.api.model.Product;
import com.noman.product.service.api.response.exception.ProductServiceException;

@RunWith(MockitoJUnitRunner.class)
public class DefaultProductDataServiceTest {
	
	@InjectMocks
	private DefaultProductDataService productDataService;
	
	@Mock
	private ProductDbService productDbService;
	
	@Test
	public void testGetProductsSuccess() throws ProductServiceException {
		Mockito.when(productDbService.getAllProducts()).thenReturn(prepareTestProducts());
		
		List<Product> products = productDataService.getProducts();
		assertEquals(2, products.size());
		Product firstProduct = products.get(0);
		Product secondProduct = products.get(1);
		
		assertNotNull(firstProduct);
		assertEquals("p1", firstProduct.getName());
		assertEquals(10.0, firstProduct.getPrice(), 0);
		
		assertNotNull(secondProduct);
		assertEquals("p2", secondProduct.getName());
		assertEquals(5.0, secondProduct.getPrice(), 0);
		
		Mockito.verify(productDbService, Mockito.times(1)).getAllProducts();
	}
	
	@Test(expected=ProductServiceException.class)
	public void testGetProductsException() throws ProductServiceException {
		Mockito.when(productDbService.getAllProducts())
			.thenThrow(new RuntimeException("error"));
		
		productDataService.getProducts();
		
		Mockito.verify(productDbService, Mockito.times(1)).getAllProducts();
	}
	
	@Test
	public void testCreateOrUpdateProductSuccess() throws ProductServiceException {
		String productName = "product1";
		double productPrice = 10.0;
		Mockito.when(productDbService.createOrUpdateProduct(productName, productPrice))
			.thenReturn(new ProductModel(productName, productPrice));
		
		Product product = productDataService.createOrUpdateProduct(new Product(productName, productPrice));
		
		assertNotNull(product);
		assertEquals(productName, product.getName());
		assertEquals(productPrice, product.getPrice(), 0);
		
		Mockito.verify(productDbService, Mockito.times(1)).createOrUpdateProduct(productName, productPrice);
	}
	
	@Test(expected=ProductServiceException.class)
	public void testCreateOrUpdateProductExceptionWhenDbServiceReturnNull() 
			throws ProductServiceException {
		String productName = "product1";
		double productPrice = 10.0;
		Mockito.when(productDbService.createOrUpdateProduct(productName, productPrice))
			.thenReturn(null);
		
		productDataService.createOrUpdateProduct(new Product(productName, productPrice));
		
		Mockito.verify(productDbService, Mockito.times(1)).createOrUpdateProduct(productName, productPrice);
	}
	
	@Test(expected=ProductServiceException.class)
	public void testCreateOrUpdateProductExceptionWhenDbServiceThrowException() 
			throws ProductServiceException {
		String productName = "product1";
		double productPrice = 10.0;
		Mockito.when(productDbService.createOrUpdateProduct(productName, productPrice))
			.thenThrow(new RuntimeException("error"));
		
		productDataService.createOrUpdateProduct(new Product(productName, productPrice));
		
		Mockito.verify(productDbService, Mockito.times(1)).createOrUpdateProduct(productName, productPrice);
	}


	private List<ProductModel> prepareTestProducts() {
		List<ProductModel> productList = new ArrayList<ProductModel>();
		productList.add(new ProductModel("p1", 10.0));
		productList.add(new ProductModel("p2", 5.0));
		return productList;
	}
}
