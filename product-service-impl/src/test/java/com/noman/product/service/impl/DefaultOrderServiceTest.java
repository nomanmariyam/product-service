package com.noman.product.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.noman.product.db.entity.OrderModel;
import com.noman.product.db.service.OrderDbService;
import com.noman.product.db.service.exception.ProductServiceDbException;
import com.noman.product.service.api.model.Order;
import com.noman.product.service.api.model.OrderRequest;
import com.noman.product.service.api.response.exception.ProductServiceException;

@RunWith(MockitoJUnitRunner.class)
public class DefaultOrderServiceTest {
	
	@InjectMocks
	private DefaultOrderService defaultOrderService;
	
	@Mock
	private OrderDbService orderDbService;
	
	@Test
	public void testCreateOrderSuccess() throws ProductServiceDbException, ProductServiceException {
		List<String> productList = Arrays.asList(StringUtils.split("p1"));
		String email = "email";
		Mockito.when(orderDbService.createOrder(
				productList, email)).thenReturn(new OrderModel(email));
		
		Order createdOrder = defaultOrderService.createOrder(new OrderRequest(productList, "email"));
		assertNotNull(createdOrder);
		assertEquals(email, createdOrder.getBuyerEmail());
		
		Mockito.verify(orderDbService, Mockito.times(1)).createOrder(productList, email);
	}
	
	@Test(expected=ProductServiceException.class)
	public void testCreateOrderWhenException() throws ProductServiceDbException, ProductServiceException {
		List<String> productList = Arrays.asList(StringUtils.split("p1"));
		String string = "email";
		Mockito.when(orderDbService.createOrder(
				productList, string)).thenThrow(new ProductServiceDbException());
		
		defaultOrderService.createOrder(new OrderRequest(productList, "email"));
		
		Mockito.verify(orderDbService, Mockito.times(1)).createOrder(productList, string);
	}
	
	@Test
	public void testGetAllOrders() throws ProductServiceDbException, ProductServiceException {
		List<OrderModel> orderModelList = new ArrayList<OrderModel>();
		orderModelList.add(new OrderModel("email1"));
		orderModelList.add(new OrderModel("email2"));
		Mockito.when(orderDbService.getAllOrders()).thenReturn(orderModelList);
		
		List<Order> allOrders = defaultOrderService.getAllOrders();
		assertNotNull(allOrders);
		assertEquals(2, allOrders.size());
		
		Mockito.verify(orderDbService, Mockito.times(1)).getAllOrders();
	}
	
	@Test(expected=ProductServiceException.class)
	public void testGetAllOrdersWhenException() throws ProductServiceDbException, ProductServiceException {
		List<OrderModel> orderModelList = new ArrayList<OrderModel>();
		orderModelList.add(new OrderModel("email1"));
		orderModelList.add(new OrderModel("email2"));
		Mockito.when(orderDbService.getAllOrders()).thenThrow(new ProductServiceDbException());
		
		defaultOrderService.getAllOrders();
		
		Mockito.verify(orderDbService, Mockito.times(1)).getAllOrders();
	}
	
	@Test
	public void testGetAllOrderBetweenTimePeriod() throws ProductServiceDbException, ProductServiceException {
		List<OrderModel> orderModelList = new ArrayList<OrderModel>();
		orderModelList.add(new OrderModel("email1"));
		orderModelList.add(new OrderModel("email2"));
		
		Date start = new Date();
		Date end = new Date();
		Mockito.when(orderDbService.getAllOrdersBetween(start, end))
			.thenReturn(orderModelList);
		
		List<Order> allOrders = defaultOrderService.getAllOrderBetweenTimePeriod(start, end);
		assertNotNull(allOrders);
		assertEquals(2, allOrders.size());
		
		Mockito.verify(orderDbService, Mockito.times(1)).getAllOrdersBetween(start, end);
	}
	
	@Test(expected=ProductServiceException.class)
	public void testGetAllOrderBetweenTimePeriodWhenException() throws ProductServiceDbException, ProductServiceException {
		List<OrderModel> orderModelList = new ArrayList<OrderModel>();
		orderModelList.add(new OrderModel("email1"));
		orderModelList.add(new OrderModel("email2"));
		
		Date start = new Date();
		Date end = new Date();
		Mockito.when(orderDbService.getAllOrdersBetween(start, end))
		.thenThrow(new ProductServiceDbException());
		
		defaultOrderService.getAllOrderBetweenTimePeriod(start, end);
		
		Mockito.verify(orderDbService, Mockito.times(1)).getAllOrdersBetween(start, end);
	}
	
	@Test
	public void testTransformOrderDbModelIntoOrder() throws ProductServiceDbException, ProductServiceException {
		List<OrderModel> orderModelList = new ArrayList<OrderModel>();
		orderModelList.add(new OrderModel("email1"));
		orderModelList.add(new OrderModel("email2"));
		orderModelList.add(new OrderModel("email3"));
		
		List<Order> allOrders = defaultOrderService.transformOrderDbModelIntoOrder(orderModelList);
		assertNotNull(allOrders);
		assertEquals(3, allOrders.size());
		assertOrderModelWithOrder( orderModelList.get(0), allOrders.get(0));
		assertOrderModelWithOrder( orderModelList.get(1), allOrders.get(1));
		assertOrderModelWithOrder( orderModelList.get(2), allOrders.get(2));
	}

	private void assertOrderModelWithOrder(OrderModel orderModel, Order order) {
		assertEquals(orderModel.getBuyerEmail(), order.getBuyerEmail());
		assertEquals(orderModel.getProducts(), order.getProducts());
		assertEquals(orderModel.getTotalPrice(), order.getPrice());
	}
}
