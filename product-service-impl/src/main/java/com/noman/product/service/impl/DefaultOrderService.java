package com.noman.product.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.noman.product.db.entity.OrderModel;
import com.noman.product.db.service.OrderDbService;
import com.noman.product.service.api.OrderService;
import com.noman.product.service.api.model.Order;
import com.noman.product.service.api.model.OrderRequest;
import com.noman.product.service.api.response.exception.ProductServiceException;

@Component
public class DefaultOrderService implements OrderService {

	@Autowired
	private OrderDbService orderDbService;

	@Override
	public Order createOrder(@Valid OrderRequest order) throws ProductServiceException {
		try {
			OrderModel createdOrderModel = 
					orderDbService.createOrder(order.getProducts(), order.getBuyerEmail());
			
			if(createdOrderModel != null) {
				return new Order(createdOrderModel.getOrderId(), createdOrderModel.getProducts(), 
						createdOrderModel.getBuyerEmail(), createdOrderModel.getTotalPrice(),
						createdOrderModel.getCreateDate());
			} 
			throw new ProductServiceException("product can not be created due to some error");
		} catch(Exception ex) {
			throw new ProductServiceException(ex.getMessage());
		}
	}

	@Override
	public List<Order> getAllOrders() throws ProductServiceException {
		try {
			
			List<OrderModel> allOrdersModel = orderDbService.getAllOrders();
			return transformOrderDbModelIntoOrder(allOrdersModel);
		} catch(Exception ex) {
			throw new ProductServiceException(ex.getMessage());
		}
	}

	@Override
	public List<Order> getAllOrderBetweenTimePeriod(@NotNull Date start, @NotNull Date end) 
			throws ProductServiceException {
		try {
			List<OrderModel> allOrdersModel = orderDbService.getAllOrdersBetween(start, end);
			return transformOrderDbModelIntoOrder(allOrdersModel);
		} catch(Exception ex) {
			throw new ProductServiceException(ex.getMessage());
		}
	}

	protected List<Order> transformOrderDbModelIntoOrder(List<OrderModel> allOrdersModel) {
		List<Order> orders = new ArrayList<Order>();
		allOrdersModel.forEach(orderModel -> orders.add(new Order(orderModel.getOrderId(), 
					orderModel.getProducts(), orderModel.getBuyerEmail(), orderModel.getTotalPrice(),
					orderModel.getCreateDate())));
		return orders;
	}
}
