package com.noman.product.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.noman.product.db.entity.ProductModel;
import com.noman.product.db.service.ProductDbService;
import com.noman.product.service.api.ProductService;
import com.noman.product.service.api.model.Product;
import com.noman.product.service.api.response.exception.ProductServiceException;

@Component
public class DefaultProductDataService implements ProductService {

	@Autowired
	private ProductDbService productDbService;
	
	@Override
	public List<Product> getProducts() throws ProductServiceException {
		try {
			List<Product> productList = new ArrayList<Product>();
			List<ProductModel> productModelList = productDbService.getAllProducts();
			productModelList.forEach(productModel -> 
				productList.add(new Product(productModel.getName(), productModel.getProductCurrentPrice())));
			return productList;
		} catch(Exception ex) {
			throw new ProductServiceException(ex.getMessage());
		}
	}

	@Override
	public Product createOrUpdateProduct(Product product) throws ProductServiceException {
		try {
			ProductModel createdProductModel = productDbService.createOrUpdateProduct(
					product.getName(), product.getPrice());
			
			if(createdProductModel != null) {
				return new Product(createdProductModel.getName(), createdProductModel.getProductCurrentPrice());
			} 
			throw new ProductServiceException("product can not be created due to some error");
		} catch(Exception ex) {
			throw new ProductServiceException(ex.getMessage());
		}
	}
}
