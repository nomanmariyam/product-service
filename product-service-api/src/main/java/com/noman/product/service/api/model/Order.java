package com.noman.product.service.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Order implements Serializable {
	
	private static final long serialVersionUID = 3526078828692303449L;

	private Long orderId;
	
	private List<String> products;
	
	private String buyerEmail;

	private Double price;
	
	private Date createDate;
	
	public Order() {
		super();
	}

	public Order(Long orderId, List<String> products, String buyerEmail, Double price, Date createDate) {
		super();
		this.orderId = orderId;
		this.products = products;
		this.buyerEmail = buyerEmail;
		this.price = price;
		this.createDate = createDate;
	}
	
	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public List<String> getProducts() {
		return products;
	}

	public void setProducts(List<String> products) {
		this.products = products;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buyerEmail == null) ? 0 : buyerEmail.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((products == null) ? 0 : products.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (buyerEmail == null) {
			if (other.buyerEmail != null)
				return false;
		}
		else if (!buyerEmail.equals(other.buyerEmail))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		}
		else if (!price.equals(other.price))
			return false;
		if (products == null) {
			if (other.products != null)
				return false;
		}
		else if (!products.equals(other.products))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", products=" + products + ", buyerEmail=" + buyerEmail + ", price="
				+ price + "]";
	}
}
