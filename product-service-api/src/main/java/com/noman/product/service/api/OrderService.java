package com.noman.product.service.api;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.noman.product.service.api.model.Order;
import com.noman.product.service.api.model.OrderRequest;
import com.noman.product.service.api.response.exception.ProductServiceException;

@Validated
@RestController
public interface OrderService {
	
	@PostMapping("/order")
	Order createOrder(@Valid @RequestBody OrderRequest order) throws ProductServiceException;
	
	@GetMapping("/order")
	List<Order> getAllOrders() throws ProductServiceException;
	
	@GetMapping("/fetchOrders")
	List<Order> getAllOrderBetweenTimePeriod(
			@NotNull @RequestParam("start") @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
			@NotNull @RequestParam("end") @DateTimeFormat(pattern="yyyy-MM-dd") Date end
			) throws ProductServiceException;
	
}
