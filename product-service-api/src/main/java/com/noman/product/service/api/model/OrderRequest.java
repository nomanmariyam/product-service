package com.noman.product.service.api.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderRequest implements Serializable {
	
	private static final long serialVersionUID = 3526078828692303449L;

	@NotNull
	private List<String> products;
	
	@NotNull
	private String buyerEmail;

	public OrderRequest() {
		super();
	}

	public OrderRequest(List<String> products, String buyerEmail) {
		super();
		this.products = products;
		this.buyerEmail = buyerEmail;
	}

	public List<String> getProducts() {
		return products;
	}

	public void setProducts(List<String> products) {
		this.products = products;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buyerEmail == null) ? 0 : buyerEmail.hashCode());
		result = prime * result + ((products == null) ? 0 : products.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderRequest other = (OrderRequest) obj;
		if (buyerEmail == null) {
			if (other.buyerEmail != null)
				return false;
		}
		else if (!buyerEmail.equals(other.buyerEmail))
			return false;
		if (products == null) {
			if (other.products != null)
				return false;
		}
		else if (!products.equals(other.products))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderRequest [products=" + products + ", buyerEmail=" + buyerEmail + "]";
	}
}
