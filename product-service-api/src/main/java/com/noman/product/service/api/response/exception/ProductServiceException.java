package com.noman.product.service.api.response.exception;


public class ProductServiceException extends Exception {

    private static final long serialVersionUID = 1L;
    
    private String action;

    public ProductServiceException(String action, String message, Throwable cause) {
        super(message, cause);
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    /**
     * Constructor to use when another exception should be wrapped.
     * @param t The exception to wrap.
     */
    public ProductServiceException(Throwable t) {
        super(t);
    }

    /**
     * Default constructor.
     */
    public ProductServiceException() {
        super();
    }

    /**
     * Constructor to use when the exception should contain additional info.
     * @param message The additional info to include.
     */
    public ProductServiceException(String message) {
        super(message);
    }

    /**
     * Constructor to wrap an exception and include additional info.
     * @param message The additional info to include.
     * @param t The exception to wrap.
     */
    public ProductServiceException(String message, Throwable t) {
        super(message, t);
    }
}
