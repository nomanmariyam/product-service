package com.noman.product.service.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.noman.product.service.api.model.Product;
import com.noman.product.service.api.response.exception.ProductServiceException;

@Validated
@RestController
public interface ProductService {
	
	@PostMapping("/products")
	Product createOrUpdateProduct(@Valid @RequestBody Product product) throws ProductServiceException;
	
	@GetMapping("/products")
	List<Product> getProducts() throws ProductServiceException;
	
}
